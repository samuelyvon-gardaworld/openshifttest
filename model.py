import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import tqdm
from torch.optim import Adam
from torch.utils.data import DataLoader

from dataset import IrisDataset


class IrisClassifier(nn.Module):

    def __init__(self):
        super().__init__()
        
        self.net = nn.Sequential(
            nn.Linear(4, 10, dtype=torch.float32),
            nn.ReLU(),
            nn.Linear(10, 10, dtype=torch.float32),
            nn.ReLU(),
            nn.Linear(10, 3, dtype=torch.float32)
        )

    def forward(self, x):
        x = self.net(x)
        
        if not self.training:
            x = F.softmax(x, -1)
        
        return x


def loss_func(y_hat, y):
    return F.cross_entropy(y_hat, y)


def accuracy(model, dataset: IrisDataset, device: str) -> float:
    model.eval()

    N = len(dataset)

    ok = 0

    for i in range(N):
        x, y = dataset[i]

        x = x.to(device)
        y = y.to(device)

        y_hat = model(x)
        
        # softmax to get the probs
        pred = torch.argmax(y_hat, -1)
        actu = torch.argmax(y, -1)

        ok += 1 if (pred == actu) else 0

    return ok / N


def train_model(model, train_dataset: IrisDataset, device: str, batch_size = 1, max_epochs = 100):
    dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)

    model = model.to(device)
    optimizer = Adam(params=model.parameters(), lr=1e-3)

    losses_accuracy = np.zeros((2, max_epochs), dtype="float32")
    
    for epoch in tqdm.trange(max_epochs):
        model.train()

        epoch_loss = torch.zeros(1, device=device)

        for minibatch in dataloader:
            optimizer.zero_grad()

            x, y = minibatch
            x = x.to(device)
            y = y.to(device)

            y_hat = model(x)

            loss = loss_func(y_hat, y)
            
            loss.backward()
            optimizer.step()

            epoch_loss += loss

        # Average loss per epoch
        epoch_loss /= len(train_dataset)
        epoch_loss = epoch_loss.cpu()

        # Accuracy
        acc = accuracy(model, train_dataset, device)

        losses_accuracy[0, epoch] = epoch_loss.detach().numpy()
        losses_accuracy[1, epoch] = acc

    return losses_accuracy
