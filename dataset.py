import torch
import numpy as np
from typing import Tuple
import pandas as pd

from sklearn.datasets import load_iris
from torch.utils.data import Dataset
import torch.nn.functional as F

COLORS_PER_CLS = ["red", "blue", "green"]
CLS_NAME = ["Iris Setosa", "Iris Versicolour", "Iris Virginica"]


def iris_dataframe() -> Tuple[pd.DataFrame, list[str]]:
    iris = load_iris()

    data = iris['data']
    features = iris["feature_names"]

    target = iris['target']
    target = np.expand_dims(target, axis=-1)

    df = pd.DataFrame(data=data, columns=features)
    df["class"] = target

    return df, features


def iris_dataframe_split(data: pd.DataFrame, train_split=0.7):
    shuffled = data.sample(frac=1)
    N = len(shuffled)

    cutoff = int(N * train_split)
    train = shuffled[:cutoff]
    test = shuffled[cutoff:]

    return train, test


class IrisDataset(Dataset):

    def __init__(self, split: pd.DataFrame, maximums):
        """

        :param split: the data
        :param maximums: the maximum of the training dataset, so we can scale features between [0,1]. No value is
                         going to be neg. here
        """
        super().__init__()
        self._data = split.to_numpy()
        self._max = maximums

    def __len__(self):
        return len(self._data)

    def __getitem__(self, item):
        row = self._data[item, :]
        x = torch.tensor(row[:4] / self._max, dtype=torch.float32)
        y = F.one_hot(torch.tensor(int(row[4])), 3).float()
        return x, y


if __name__ == '__main__':
    data, _ = iris_dataframe()
    ds = IrisDataset(data, data.max()[:4].to_numpy())
    print(ds[0])
